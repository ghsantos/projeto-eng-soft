CREATE schema radiotaxi;
USE radiotaxi;

CREATE TABLE funcionario (
  id int(10) NOT NULL AUTO_INCREMENT,
  nome varchar(100) NOT NULL,
  tipo varchar(15) NOT NULL,
  cpf varchar(15) NOT NULL,
  email varchar(45) NOT NULL,
  senha varchar(45) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE cliente (
  codigo int(10) NOT NULL AUTO_INCREMENT,
  nome varchar(100) NOT NULL,
  telefone varchar(11) NOT NULL,
  PRIMARY KEY (codigo)
); 

CREATE TABLE endereco(
  id int(10) NOT NULL AUTO_INCREMENT,
  logradouro varchar(50) NOT NULL,
  numero int(10) unsigned NOT NULL,
  complemento varchar(50) NOT NULL,
  bairro varchar(50) NOT NULL,
  municipio varchar(50) NOT NULL,
  estado varchar(50) NOT NULL,
  codigo_cliente int(10) NOT NULL,
  PRIMARY KEY (id),
  KEY fk_endereco_cliente (codigo_cliente),
  CONSTRAINT fk_endereco_cliente FOREIGN KEY (codigo_cliente) REFERENCES cliente(codigo)
);

CREATE TABLE cooperado(
  vr int(10) NOT NULL AUTO_INCREMENT,
  cpf varchar(11) NOT NULL,
  nome varchar(50) NOT NULL,
  num_carteira varchar(11) NOT NULL,
  data_val_carteira date NOT NULL,
  categoria varchar(50) NOT NULL,
  endereco varchar(50) NOT NULL,
  telefone_res varchar(10) NOT NULL,
  telefone_cel varchar(11) NOT NULL,
  data_entrada date NOT NULL,
  PRIMARY KEY (vr)
);

CREATE TABLE veiculo (
  placa varchar(7) NOT NULL,
  modelo varchar(50) NOT NULL,
  fabricante varchar(50) NOT NULL,
  cor varchar(50) NOT NULL,
  cooperado_vr int(10) NOT NULL,
  PRIMARY KEY (placa),
  CONSTRAINT fk_veiculo_cooperado FOREIGN KEY(cooperado_vr) REFERENCES cooperado(vr)
);

CREATE TABLE corrida(
  codigo int(10) NOT NULL AUTO_INCREMENT,
  endereco_saida varchar(50) NOT NULL,
  endereco_destino varchar(50) NOT NULL,
  dataHora_saida DATETIME NOT NULL,
  condicao varchar(50) NOT NULL,
  cooperado_vr int(10) NOT NULL,
  cliente_codigo int(10) NOT NULL,
  PRIMARY KEY (codigo),
  KEY fk_corrida_cooperado (cooperado_vr),
  KEY fk_corrida_cliente (cliente_codigo),
  CONSTRAINT fk_corrida_cooperado FOREIGN KEY (cooperado_vr) REFERENCES cooperado(vr),
  CONSTRAINT fk_corrida_cliente FOREIGN KEY (cliente_codigo) REFERENCES cliente(codigo)
);

/*
Funcionarios

tipos: administrador, financeiro, operador
*/

INSERT INTO funcionario (nome, tipo, cpf, email, senha)
values ('Guilherme Noah Barros', 'administrador', '59145372748', 'gbarros@hotmail.com', 'teste1234');

INSERT INTO funcionario (nome, tipo, cpf, email, senha)
values ('Raul Jorge Benedito Araújo', 'financeiro', '65259340442', 'raularaujo@gmail.com', 'teste1234');

/*
INSERT INTO funcionario (nome, tipo, cpf, email, senha)
values ('', '', '', '', '');
*/

/*
Clientes
*/

INSERT INTO cliente (nome, telefone) values ('Luan Elias dos Santos', '61995504430');
INSERT INTO cliente (nome, telefone) values ('Gabrielly Daiane Ferreira', '61988453800');
INSERT INTO cliente (nome, telefone) values ('Antonio Diogo Moura', '61985650730');
INSERT INTO cliente (nome, telefone) values ('Kauê Pedro Luan da Cunha', '61997186651');

/*
Endereços
*/

INSERT INTO endereco (logradouro, numero, complemento, bairro, municipio, estado, codigo_cliente)
values ('Núcleo Rural de Santa Maria', 894, '', 'Zona de Dinamização (Santa Maria)', 'Brasília', 'Distrito Federal', 1);

INSERT INTO endereco (logradouro, numero, complemento, bairro, municipio, estado, codigo_cliente)
values ('Quadra 27', 282, 'Conjunto C', 'Paranoá', 'Brasília', 'Distrito Federal', 2);

INSERT INTO endereco (logradouro, numero, complemento, bairro, municipio, estado, codigo_cliente)
values ('Quadra SHIS QL 24', 440, '', 'Setor de Habitações Individuais Sul', 'Brasília', 'Distrito Federal', 3);

INSERT INTO endereco (logradouro, numero, complemento, bairro, municipio, estado, codigo_cliente)
values ('Quadra QS 417 Conjunto H', 174, '', 'Samambaia Norte (Samambaia)', 'Brasília', 'Distrito Federal', 4);

/*
cooperados
*/

INSERT INTO cooperado (cpf, nome, num_carteira, data_val_carteira, categoria, endereco, telefone_res, telefone_cel, data_entrada)
values ('18442266119', 'Hugo Tomás Silva', '44970832313', '2019-04-06', 'B', 'Quadra Quadra 8A, Incra 8 (Brazlândia)', '6128784520', '61995463524', '2017-02-19');

INSERT INTO cooperado (cpf, nome, num_carteira, data_val_carteira,
categoria, endereco, telefone_res, telefone_cel, data_entrada)
values ('04425786106', 'Mário Noah Ribeiro', '20742123840', '2019-01-04', 'B', 'Rodovia DF-150 Km 2,5 Grande Colorado (Sobradinho)', '6128371593', '61996617834', '2017-02-19');

INSERT INTO cooperado (cpf, nome, num_carteira, data_val_carteira,
categoria, endereco, telefone_res, telefone_cel, data_entrada)
values ('92024482139', 'Kaique Diego da Silva', '49989475470', '2020-09-09', 'B', 'Quadra QSC 2 Taguatinga Sul (Taguatinga)', '6139485611', '61998924316', '2017-02-19');

/*
veiculos
*/

INSERT INTO veiculo (placa, modelo, fabricante, cor, cooperado_vr)
values('JEL4930', 'Vectra GLS', 'GM - Chevrolet', 'Branco', 1);

INSERT INTO veiculo (placa, modelo, fabricante, cor, cooperado_vr)
values('JFC8615', 'Pajero Sport', 'Mitsubishi', 'Cinza', 2);

INSERT INTO veiculo (placa, modelo, fabricante, cor, cooperado_vr)
values('JHF6785', 'Impreza GL', 'Mitsubishi', 'Azul', 3);

/*
corridas
*/

INSERT INTO corrida (endereco_saida, endereco_destino, dataHora_saida, condicao, cooperado_vr, cliente_codigo)
values ('Rua 8 Chácara 11A, Vila São José (Vicente Pires)', 'Quadra QMS 6, Setor de Mansões de Sobradinho',
'2018-07-01 12:30:00', 'Programada', 2, 1);

INSERT INTO corrida (endereco_saida, endereco_destino, dataHora_saida, condicao, cooperado_vr, cliente_codigo)
values ('Quadra QMS 6, Setor de Mansões de Sobradinho', 'Rua 8 Chácara 11A, Vila São José (Vicente Pires)',
'2018-07-01 14:30:00', 'Programada', 2, 3);

