const knex = require('knex')(require('./knexfile'));

module.exports = {
  authenticate({ email, senha }) {
    return knex('funcionario').where({ email })
      .then(([user]) => {
        if (!user) {
          return { success: false, message: 'Email não cadastrado' }
        }

        if (user.senha === senha) {
          const { senha, ...funcionario } = user;
          return { success: true, funcionario }
        }
        return { success: false, message: 'Senha incorreta' };
      }).catch(err => console.log(err));
  },

  insertCliente(cliente) {
    return knex('cliente').insert({ nome: cliente.nome, telefone: cliente.telefone })
      .returning(['codigo'])
      .then(([codigo]) => {
        return knex('endereco').insert({
          logradouro: cliente.logradouro,
          numero: cliente.numero,
          complemento: cliente.complemento,
          bairro: cliente.complemento,
          municipio: cliente.bairro,
          estado: cliente.estado,
          codigo_cliente: codigo,
        });
      }).then(() => { success: true });
  },

  getClientes() {
    return knex('cliente')
      .then(clientes => {
        return clientes;
      });
  },

  getEnderecos() {
    return knex('endereco')
      .then(enderecos => {
        return enderecos;
      });
  },

  insertCooperado(cooperado) {
    return knex('cooperado').insert({
      cpf: cooperado.cpf,
      nome: cooperado.nome,
      num_carteira: cooperado.num_carteira,
      data_val_carteira: cooperado.data_val_carteira,
      categoria: cooperado.categoria,
      endereco: cooperado.endereco,
      telefone_res: cooperado.telefone_res,
      telefone_cel: cooperado.telefone_cel,
      data_entrada: cooperado.data_entrada,
    }).returning(['vr'])
    .then(([vr]) => {
      return knex('veiculo').insert({
        placa: cooperado.placa,
        modelo: cooperado.modelo,
        fabricante: cooperado.fabricante,
        cor: cooperado.cor,
        cooperado_vr: vr,
      });
    }).then(() => { success: true });
  },

  getCooperados() {
    return knex('cooperado')
      .then(cooperados => {
        return cooperados;
      });
  },

  getVeiculos() {
    return knex('veiculo')
      .then(veiculos => {
        return veiculos;
      });
  },

  getCorridas() {
    return knex('corrida')
      .then(corridas => {
        return corridas;
      });
  }
};
