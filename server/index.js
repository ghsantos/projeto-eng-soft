const express = require('express');
const bodyParser = require('body-parser');

const store = require('./store');

const app = express();

/** set up middlewares */
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', (req, res) => {
  res.send('hello');
});

app.post('/login', (req, res) => {
  store.authenticate({ email: req.body.email, senha: req.body.senha })
    .then((result) => {
      if (result.success) {
        res.status(200).send({ funcionario: result.funcionario });
      } else {
        res.status(401).send({ message: result.message });
      }
    }).catch(err => {
      console.log(err);
      res.sendStatus(500);
    });
});

app.get('/cliente', (req, res) => {
  store.getClientes()
    .then((clientes) => {
      res.status(200).send(clientes);
    }).catch(err => {
      console.log(err);
      res.sendStatus(500);
    });
});

app.post('/cliente', (req, res) => {
    console.log(req.body);
    store.insertCliente(req.body)
      .then(() => res.sendStatus(201))
      .catch((err) => {
        console.log(err);
        res.status(500).send({ message: 'Erro no servidor :(' });
      })
});

app.get('/endereco', (req, res) => {
  store.getEnderecos()
    .then((enderecos) => {
      res.status(200).send(enderecos);
    }).catch(err => {
      console.log(err);
      res.sendStatus(500);
    });
});

app.get('/cooperado', (req, res) => {
  store.getCooperados()
    .then((cooperados) => {
      res.status(200).send(cooperados);
    }).catch(err => {
      console.log(err);
      res.sendStatus(500);
    });
});

app.post('/cooperado', (req, res) => {
    console.log(req.body);
    store.insertCooperado(req.body)
      .then(() => res.sendStatus(201))
      .catch((err) => {
        console.log(err);
        res.status(500).send({ message: 'Erro no servidor :(' });
      })
});

app.get('/veiculo', (req, res) => {
  store.getVeiculos()
    .then((veiculos) => {
      res.status(200).send(veiculos);
    }).catch(err => {
      console.log(err);
      res.sendStatus(500);
    });
});

app.get('/corrida', (req, res) => {
  store.getCorridas()
    .then((corridas) => {
      res.status(200).send(corridas);
    }).catch(err => {
      console.log(err);
      res.sendStatus(500);
    });
});

const port = 8080 || process.env.PORT;

app.listen(port, '0.0.0.0', () => {
  console.log(`Server started at port ${port}`);
});
