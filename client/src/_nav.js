export default {
  items: [
    {
      name: 'Corridas',
      url: '/administracao',
      icon: 'icon-speedometer',
    },
    {
      name: 'Clientes',
      url: '/clientes',
      icon: 'icon-people',
    },
    {
      name: 'Cooperados',
      url: '/cooperados',
      icon: 'icon-pie-chart',
    },
  ],
};
