import React from 'react';
import { Line } from 'react-chartjs-2';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setCooperados, setVeiculos, setClientes, setEnderecos, setCorridas } from '../../actions';
import {
  Alert,
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Input,
  InputGroup,
  InputGroupAddon,
  Progress,
  Row,
  Table,
} from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle } from '@coreui/coreui/dist/js/coreui-utilities'
import http from '../../lib/http';

const brandSuccess = getStyle('--success')
const brandInfo = getStyle('--info')
const brandDanger = getStyle('--danger')

//Random Numbers
function random(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

var elements = 7;
var data1 = [];
var data2 = [];

for (var i = 0; i < elements; i++) {
  data1.push(random(50, 200));
  data2.push(random(350, 600));
}

const mainChart = {
  labels: ['Sáb', 'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex'],
  datasets: [
    {
      label: 'Número de corridas:',
      backgroundColor: 'transparent',
      borderColor: brandSuccess,
      pointHoverBackgroundColor: '#fff',
      borderWidth: 2,
      data: data1,
    },
    {
      label: 'Faturamento R$',
      backgroundColor: 'transparent',
      borderColor: brandDanger,
      pointHoverBackgroundColor: '#fff',
      borderWidth: 2,
      data: data2,
    },
  ],
};

const mainChartOpts = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips,
    intersect: true,
    mode: 'index',
    position: 'nearest',
    callbacks: {
      labelColor: function(tooltipItem, chart) {
        return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor }
      }
    }
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          drawOnChartArea: false,
        },
      }],
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
          maxTicksLimit: 5,
          stepSize: Math.ceil(600 / 6),
          max: 600,
        },
      }],
  },
  elements: {
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4,
      hoverBorderWidth: 3,
    },
  },
};

class Administracao extends React.Component {
  state = {
    busca: '',
    addVisisble: false,
    error: false,
    errorMgs: '',
    dropdownOpen: false,
    radioSelected: 1,
  };

  componentDidMount() {
    this.getCooperados();
    this.getVeiculos();
    this.getClientes();
    this.getEnderecos();
    this.getCorridas();
  }

  getCooperados() {
    http.get('/cooperado')
    .then((res) => {
      this.props.setCooperados(res.data);
    }).catch(err => console.log(err));
  }

  getVeiculos() {
    http.get('/veiculo')
    .then((res) => {
      this.props.setVeiculos(res.data);
    }).catch(err => console.log(err));
  }

  getClientes() {
    http.get('/cliente')
    .then((res) => {
      this.props.setClientes(res.data);
    }).catch(err => console.log(err));
  }

  getEnderecos() {
    http.get('/endereco')
    .then((res) => {
      this.props.setEnderecos(res.data);
    }).catch(err => console.log(err));
  }

  getCorridas() {
    http.get('/corrida')
    .then((res) => {
      this.props.setCorridas(res.data);
    }).catch(err => console.log(err));
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected,
    });
  }

  handleChange(event) {
    this.setState({ busca: event.target.value });
  }

  render() {
    let corridas = [];

    if (this.state.busca.length) {
      corridas = this.props.corridas.filter(corrida =>
        !!corrida.cooperado_vr.toString().match(this.state.busca) ||
        !!corrida.codigo.toString().match(this.state.busca));
    } else {
      if (!!this.props.corridas) {
        corridas = this.props.corridas;
      }
    }

    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardBody>
                <Row>
                  <Col sm="5">
                    <CardTitle className="mb-0">Corridas</CardTitle>
                    <div className="small text-muted">Junho de 2018</div>
                  </Col>
                  <Col sm="7" className="d-none d-sm-inline-block">
                    <ButtonToolbar className="float-right" aria-label="Toolbar with button groups">
                      <ButtonGroup className="mr-3" aria-label="First group">
                        <Button color="outline-secondary" onClick={() => this.onRadioBtnClick(1)} active={this.state.radioSelected === 1}>Semama</Button>
                        <Button color="outline-secondary" onClick={() => this.onRadioBtnClick(2)} active={this.state.radioSelected === 2}>Mês</Button>
                        <Button color="outline-secondary" onClick={() => this.onRadioBtnClick(3)} active={this.state.radioSelected === 3}>Ano</Button>
                      </ButtonGroup>
                    </ButtonToolbar>
                  </Col>
                </Row>

                <div className="chart-wrapper" style={{ height: 320 + 'px', marginTop: 40 + 'px' }}>
                  <Line data={mainChart} options={mainChartOpts} height={300} />
                </div>
              </CardBody>

              <CardFooter>
                <Row className="text-center">
                  <Col sm={12} md className="mb-sm-2 mb-0">
                    <div className="text-muted">Cooperado A</div>
                    <strong>200 corridas (40%)</strong>
                    <Progress className="progress-xs mt-2" color="indigo" value="40" />
                  </Col>
                  <Col sm={12} md className="mb-sm-2 mb-0 d-md-down-none">
                    <div className="text-muted">Cooperado B</div>
                    <strong>90 corridas (18%)</strong>
                    <Progress className="progress-xs mt-2" color="cyan" value="18" />
                  </Col>
                  <Col sm={12} md className="mb-sm-2 mb-0">
                    <div className="text-muted">Cooperado C</div>
                    <strong>59 corridas (10.9%)</strong>
                    <Progress className="progress-xs mt-2" color="pink" value="10" />
                  </Col>
                  <Col sm={12} md className="mb-sm-2 mb-0">
                    <div className="text-muted">Cooperado D</div>
                    <strong>28 corridas (5.3%)</strong>
                    <Progress className="progress-xs mt-2" color="purple" value="5" />
                  </Col>
                  <Col sm={12} md className="mb-sm-2 mb-0 d-md-down-none">
                    <div className="text-muted">Cooperado E</div>
                    <strong>26 corridas (5%)</strong>
                    <Progress className="progress-xs mt-2" color="teal" value="5" />
                  </Col>
                </Row>
              </CardFooter>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col md="9">
            <Card>
              {this.state.addVisisble ?
                <CardBody>


                  { this.state.error &&
                    <Alert className="mb-4" color="danger">
                      {this.state.errorMgs}
                    </Alert>
                  }
                </CardBody>
              :
                <CardBody>
                  <InputGroup>
                    <Input
                      type="text"
                      value={this.state.busca}
                      onChange={(event) => this.handleChange(event)}
                      id="input1-group2"
                      name="input1-group2"
                      placeholder="Corrida"
                    />
                    <InputGroupAddon addonType="append">
                      <Button type="button" color="primary">
                        <i className="fa fa-search"></i> Procurar
                      </Button>
                    </InputGroupAddon>
                  </InputGroup>
                </CardBody>
              }
            </Card>
          </Col>

          <Col md="3">
            <Card>
              <CardBody>
                <Button
                  onClick={() => this.setState((prevState) => ({ addVisisble: !prevState.addVisisble, error: false }))}
                  block
                  color="success"
                >
                  { this.state.addVisisble ? 'Fechar' : 'Adicionar Corrida' }
                </Button>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader>
                Produtos
              </CardHeader>
              <CardBody>
                <Table responsive striped>
                  <thead>
                  <tr>
                    <th>Código</th>
                    <th>Data/Hora início</th>
                    <th>Nome do cliente</th>
                    <th>VR do cooperado</th>
                    <th>Condição</th>
                  </tr>
                  </thead>
                  <tbody>
                    {
                      !!this.props.clientes.length && corridas.map(corrida => {
                        const [cliente] = this.props.clientes.filter(
                          c => c.codigo === corrida.cliente_codigo
                        );

                        return (
                          <tr key={corrida.codigo}>
                            <td>{corrida.codigo}</td>
                            <td>{corrida.dataHora_saida.toLocaleString()}</td>
                            <td>{cliente && cliente.nome}</td>
                            <td>{corrida.cooperado_vr}</td>
                            <td>{corrida.condicao}</td>
                          </tr>
                        );
                      })
                    }
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}


const mapStateToProps = store => ({
  clientes: store.clientesReducer.clientes,
  cooperados: store.cooperadosReducer.cooperados,
  enderecos: store.enderecosReducer.enderecos,
  veiculos: store.veiculosReducer.veiculos,
  corridas: store.corridasReducer.corridas,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ setCooperados, setVeiculos, setClientes, setEnderecos, setCorridas }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Administracao);
