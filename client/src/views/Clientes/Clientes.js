import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setClientes, setEnderecos } from '../../actions';
import {
  Alert,
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  Table
} from 'reactstrap';
import { withFormik } from 'formik';
import * as Yup from 'yup';

import http from '../../lib/http';

const ClienteForm = (props) => {
  const {
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
  } = props;

  return(
    <form onSubmit={handleSubmit}>
      <h5>Cliente</h5>

      <InputGroup className="mb-3">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="icon-layers"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name="nome"
          type="text"
          placeholder="Nome"
          value={values.nome}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.nome && touched.nome}
        />
        <FormFeedback>Digite um nome válido</FormFeedback>
      </InputGroup>

      <InputGroup className="mb-3">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="fa fa-comment-o"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name="telefone"
          type="text"
          placeholder="Telefone"
          value={values.telefone}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.telefone && touched.telefone}
        />
        <FormFeedback>Digite um telefone válido</FormFeedback>
      </InputGroup>

      <h5>Endereço</h5>

      <InputGroup className="mb-3">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="fa fa-comment-o"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name="logradouro"
          type="text"
          placeholder="Logradouro"
          value={values.logradouro}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.logradouro && touched.logradouro}
        />
        <FormFeedback>Digite um logradouro válido</FormFeedback>
      </InputGroup>

      <InputGroup className="mb-3">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="fa fa-comment-o"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name="numero"
          type="number"
          placeholder="Número"
          value={values.numero}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.numero && touched.numero}
        />
        <FormFeedback>Digite um valor válido</FormFeedback>
      </InputGroup>

      <InputGroup className="mb-3">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="fa fa-comment-o"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name="complemento"
          type="text"
          placeholder="Complemento"
          value={values.complemento}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.complemento && touched.complemento}
        />
        <FormFeedback>Digite um complemento válido</FormFeedback>
      </InputGroup>

      <InputGroup className="mb-3">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="fa fa-comment-o"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name="bairro"
          type="text"
          placeholder="Bairro"
          value={values.bairro}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.bairro && touched.bairro}
        />
        <FormFeedback>Digite um nome válido</FormFeedback>
      </InputGroup>

      <InputGroup className="mb-3">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="fa fa-comment-o"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name="municipio"
          type="text"
          placeholder="Municipio"
          value={values.municipio}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.municipio && touched.municipio}
        />
        <FormFeedback>Digite um nome válido</FormFeedback>
      </InputGroup>

      <InputGroup className="mb-3">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="fa fa-comment-o"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name="estado"
          type="text"
          placeholder="Estado"
          value={values.estado}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.estado && touched.estado}
        />
        <FormFeedback>Digite um nome válido</FormFeedback>
      </InputGroup>

      <Button type="submit" color="primary" className="px-4" disabled={isSubmitting}>
        {isSubmitting ? 'Aguarde' : 'Adicionar'}
      </Button>

    </form>
  );
}

const InputFormik = withFormik({
  mapPropsToValues: () => ({
    nome: '',
    telefone: '',
    logradouro: '',
    numero: null,
    complemento: '',
    bairro: '',
    municipio: '',
    estado: '',
  }),

  validationSchema: Yup.object().shape({
    nome: Yup.string().min(4).max(45).required('Nome is required!'),
    telefone: Yup.string().min(10).max(11).matches(/^\d+$/).required('Telefone is required!'),
    logradouro: Yup.string().min(5).max(50).required('logradouro is required!'),
    numero: Yup.number().min(0).max(10000).required('numero is required!'),
    complemento: Yup.string().min(5).max(50).required('complemento is required!'),
    bairro: Yup.string().min(5).max(50).required('bairro is required!'),
    municipio: Yup.string().min(5).max(50).required('municipio is required!'),
    estado: Yup.string().min(5).max(50).required('estado is required!'),
  }),

  handleSubmit: (values, { setSubmitting, props }) =>
    props.onSubmit(values, setSubmitting),
})(ClienteForm);

class Clientes extends React.Component {
  state = {
    busca: '',
    addVisisble: false,
    error: false,
    errorMgs: '',
  }

  componentDidMount() {
    this.getClientes();
    this.getEnderecos();
  }

  handleChange(event) {
    this.setState({ busca: event.target.value });
  }

  getClientes() {
    http.get('/cliente')
    .then((res) => {
      this.props.setClientes(res.data);
    }).catch(err => console.log(err));
  }

  getEnderecos() {
    http.get('/endereco')
    .then((res) => {
      this.props.setEnderecos(res.data);
    }).catch(err => console.log(err));
  }

  submitProduto = (values, setSubmitting) => {
    http.post('/cliente', {
      ...values,
    }).then((res) => {
      setSubmitting(false);

      this.setState({ addVisisble: false, error: false });
      this.getClientes();
      this.getEnderecos();
    }).catch(err => {
      setSubmitting(false);

      console.log(err);
      this.setState({ error: true, errorMgs: err.response.data.mensage });
    })
  }

  render() {
    let clientes = [];

    if (this.state.busca.length) {
      clientes = this.props.clientes.filter(cliente =>
        !!cliente.nome.toLowerCase().match(this.state.busca.toLowerCase()) ||
        cliente.codigo.toString().match(this.state.busca.toLowerCase()));
    } else {
      clientes = this.props.clientes;
    }

    return (
      <div className="animated fadeIn">
        <Row>
          <Col md="9">
            <Card>
              {this.state.addVisisble ?
                <CardBody>
                  <InputFormik onSubmit={this.submitProduto} />
                  { this.state.error &&
                    <Alert className="mb-4" color="danger">
                      {this.state.errorMgs}
                    </Alert>
                  }
                </CardBody>
              :
                <CardBody>
                  <InputGroup>
                    <Input
                      type="text"
                      value={this.state.busca}
                      onChange={(event) => this.handleChange(event)}
                      id="input1-group2"
                      name="input1-group2"
                      placeholder="Cliente"
                    />
                    <InputGroupAddon addonType="append">
                      <Button type="button" color="primary">
                        <i className="fa fa-search"></i> Procurar
                      </Button>
                    </InputGroupAddon>
                  </InputGroup>
                </CardBody>
              }
            </Card>
          </Col>

          <Col md="3">
            <Card>
              <CardBody>
                <Button
                  onClick={() => this.setState((prevState) => ({ addVisisble: !prevState.addVisisble, error: false }))}
                  block
                  color="success"
                >
                  { this.state.addVisisble ? 'Fechar' : 'Adicionar Cliente' }
                </Button>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader>
                Produtos
              </CardHeader>
              <CardBody>
                <Table responsive striped>
                  <thead>
                  <tr>
                    <th>Código</th>
                    <th>Nome</th>
                    <th>Telefone</th>
                    <th>Endereço</th>
                    <th>Município</th>
                  </tr>
                  </thead>
                  <tbody>
                    {
                      !!this.props.enderecos.length && clientes.map(cliente => {
                        const [endereco] = this.props.enderecos.filter(
                          e => e.codigo_cliente === cliente.codigo
                        );

                        return (
                          <tr key={cliente.codigo}>
                            <td>{cliente.codigo}</td>
                            <td>{cliente.nome}</td>
                            <td>
                              {cliente.telefone.replace(/^(\d\d)(\d)/g,"($1) $2").replace(/(\d{5})(\d)/,"$1-$2")}
                            </td>
                            <td>{endereco && endereco.logradouro} nº {endereco && endereco.numero}</td>
                            <td>{endereco && endereco.municipio}</td>
                          </tr>
                        );
                      })
                    }
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  clientes: store.clientesReducer.clientes,
  enderecos: store.enderecosReducer.enderecos,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ setClientes, setEnderecos }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Clientes);
