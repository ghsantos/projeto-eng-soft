import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setCooperados, setVeiculos } from '../../actions';
import {
  Alert,
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Label,
  Row,
  Table
} from 'reactstrap';
import { withFormik } from 'formik';
import * as Yup from 'yup';

import http from '../../lib/http';

const CooperadoForm = (props) => {
  const {
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
  } = props;

  return(
    <form onSubmit={handleSubmit}>
      <h5>Cooperado</h5>

      <InputGroup className="mb-3">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="icon-layers"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name="cpf"
          type="text"
          placeholder="CPF"
          value={values.cpf}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.cpf && touched.cpf}
        />
        <FormFeedback>Digite um cpf válido</FormFeedback>
      </InputGroup>

      <InputGroup className="mb-3">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="icon-layers"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name="nome"
          type="text"
          placeholder="Nome"
          value={values.nome}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.nome && touched.nome}
        />
        <FormFeedback>Digite um nome válido</FormFeedback>
      </InputGroup>

      <InputGroup className="mb-3">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="icon-layers"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name="num_carteira"
          type="text"
          placeholder="Número da carteira"
          value={values.num_carteira}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.num_carteira && touched.num_carteira}
        />
        <FormFeedback>Digite um número válido</FormFeedback>
      </InputGroup>

      <InputGroup className="mb-3">
        <Label>Data de validade da carteira</Label>
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="icon-layers"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name="data_val_carteira"
          type="date"
          placeholder="Data de validade da carteira"
          value={values.data_val_carteira}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.data_val_carteira && touched.data_val_carteira}
         />
        <FormFeedback>Digite uma data válida</FormFeedback>
      </InputGroup>

      <InputGroup className="mb-3">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="icon-layers"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name="categoria"
          type="text"
          placeholder="Categoria"
          value={values.categoria}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.categoria && touched.categoria}
        />
        <FormFeedback>Digite uma categoria válida</FormFeedback>
      </InputGroup>

      <InputGroup className="mb-3">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="icon-layers"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name="endereco"
          type="text"
          placeholder="Endereco"
          value={values.endereco}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.endereco && touched.endereco}
        />
        <FormFeedback>Digite um endereco válido</FormFeedback>
      </InputGroup>

      <InputGroup className="mb-3">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="fa fa-comment-o"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name="telefone_res"
          type="text"
          placeholder="Telefone Residencial"
          value={values.telefone_res}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.telefone_res && touched.telefone_res}
        />
        <FormFeedback>Digite um telefone válido</FormFeedback>
      </InputGroup>

      <InputGroup className="mb-3">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="fa fa-comment-o"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name="telefone_cel"
          type="text"
          placeholder="Telefone Celular"
          value={values.telefone_cel}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.telefone_cel && touched.telefone_cel}
        />
        <FormFeedback>Digite um telefone válido</FormFeedback>
      </InputGroup>

      <InputGroup className="mb-3">
        <Label>Data de entrada</Label>
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="icon-layers"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name="data_entrada"
          type="date"
          placeholder="Data de entrada"
          value={values.data_entrada}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.data_entrada && touched.data_entrada}
         />
        <FormFeedback>Digite uma data válida</FormFeedback>
      </InputGroup>

      <h5>Veiculo</h5>

      <InputGroup className="mb-3">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="fa fa-comment-o"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name="placa"
          type="text"
          placeholder="Placa"
          value={values.placa}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.placa && touched.placa}
        />
        <FormFeedback>Digite uma placa válida</FormFeedback>
      </InputGroup>

      <InputGroup className="mb-3">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="fa fa-comment-o"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name="modelo"
          type="text"
          placeholder="Modelo"
          value={values.modelo}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.modelo && touched.modelo}
        />
        <FormFeedback>Digite um modelo válido</FormFeedback>
      </InputGroup>

      <InputGroup className="mb-3">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="fa fa-comment-o"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name="fabricante"
          type="text"
          placeholder="Fabricante"
          value={values.fabricante}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.fabricante && touched.fabricante}
        />
        <FormFeedback>Digite um nome válido</FormFeedback>
      </InputGroup>

      <InputGroup className="mb-3">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="fa fa-comment-o"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name="cor"
          type="text"
          placeholder="Cor"
          value={values.cor}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.cor && touched.cor}
        />
        <FormFeedback>Digite uma cor válida</FormFeedback>
      </InputGroup>

      <Button type="submit" color="primary" className="px-4" disabled={isSubmitting}>
        {isSubmitting ? 'Aguarde' : 'Adicionar'}
      </Button>

    </form>
  );
}

const InputFormik = withFormik({
  mapPropsToValues: () => ({
    cpf: '',
    nome: '',
    num_carteira: '',
    data_val_carteira: '',
    categoria: '',
    endereco: '',
    telefone_res: '',
    telefone_cel: '',
    data_entrada: '',
    placa: '',
    modelo: '',
    fabricante:  '',
    cor: '',
  }),

  validationSchema: Yup.object().shape({
    cpf: Yup.string().min(11).max(11).matches(/^\d+$/).required('CPF is required!'),
    nome: Yup.string().min(4).max(50).required('Nome is required!'),
    num_carteira: Yup.string().min(11).max(11).matches(/^\d+$/).required('num_carteira is required!'),
    data_val_carteira: Yup.date(),
    categoria: Yup.string().min(1).max(50).required('categoria is required!'),
    endereco: Yup.string().min(5).max(50).required('endereco is required!'),
    telefone_res: Yup.string().min(10).max(10).matches(/^\d+$/).required('telefone_res is required!'),
    telefone_cel: Yup.string().min(11).max(11).matches(/^\d+$/).required('telefone_cel is required!'),
    data_entrada: Yup.date(),
    placa: Yup.string().min(7).max(7).required('placa is required!'),
    modelo: Yup.string().min(1).max(50).required('modelo is required!'),
    fabricante: Yup.string().min(5).max(50).required('fabricante is required!'),
    cor: Yup.string().min(2).max(50).required('cor is required!'),
  }),

  handleSubmit: (values, { setSubmitting, props }) =>
    props.onSubmit(values, setSubmitting),
})(CooperadoForm);

class Cooperados extends React.Component {
  state = {
    busca: '',
    addVisisble: false,
    error: false,
    errorMgs: '',
  }

  componentDidMount() {
    this.getCooperados();
    this.getVeiculos();
  }

  handleChange(event) {
    this.setState({ busca: event.target.value });
  }

  getCooperados() {
    http.get('/cooperado')
    .then((res) => {
      this.props.setCooperados(res.data);
    }).catch(err => console.log(err));
  }

  getVeiculos() {
    http.get('/veiculo')
    .then((res) => {
      this.props.setVeiculos(res.data);
    }).catch(err => console.log(err));
  }

  submitCooperado = (values, setSubmitting) => {
    console.log(values);

    http.post('/cooperado', {
      ...values,
    }).then((res) => {
      setSubmitting(false);

      this.setState({ addVisisble: false, error: false });
      this.getCooperados();
      this.getVeiculos();
    }).catch(err => {
      setSubmitting(false);

      console.log(err);
      this.setState({ error: true, errorMgs: err.response.data.mensage });
    })
  }

  render() {
    let cooperados = [];

    if (this.state.busca.length) {
      cooperados = this.props.cooperados.filter(cooperado =>
        !!cooperado.nome.toLowerCase().match(this.state.busca.toLowerCase()) ||
        cooperado.vr.toString().match(this.state.busca.toLowerCase()));
    } else {
      cooperados = this.props.cooperados;
    }

    return (
      <div className="animated fadeIn">
        <Row>
          <Col md="9">
            <Card>
              {this.state.addVisisble ?
                <CardBody>
                  <InputFormik onSubmit={this.submitCooperado} />
                  { this.state.error &&
                    <Alert className="mb-4" color="danger">
                      {this.state.errorMgs}
                    </Alert>
                  }
                </CardBody>
              :
                <CardBody>
                  <InputGroup>
                    <Input
                      type="text"
                      value={this.state.busca}
                      onChange={(event) => this.handleChange(event)}
                      id="input1-group2"
                      name="input1-group2"
                      placeholder="Cooperado"
                    />
                    <InputGroupAddon addonType="append">
                      <Button type="button" color="primary">
                        <i className="fa fa-search"></i> Procurar
                      </Button>
                    </InputGroupAddon>
                  </InputGroup>
                </CardBody>
              }
            </Card>
          </Col>

          <Col md="3">
            <Card>
              <CardBody>
                <Button
                  onClick={() => this.setState((prevState) => ({ addVisisble: !prevState.addVisisble, error: false }))}
                  block
                  color="success"
                >
                  { this.state.addVisisble ? 'Fechar' : 'Adicionar Cooperado' }
                </Button>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader>
                Produtos
              </CardHeader>
              <CardBody>
                <Table responsive striped>
                  <thead>
                  <tr>
                    <th>VR</th>
                    <th>CPF</th>
                    <th>Nome</th>
                    <th>Tel Residencial</th>
                    <th>Tel Celular</th>
                    <th>Veiculo</th>
                    <th>Placa</th>
                    <th>Cor</th>
                  </tr>
                  </thead>
                  <tbody>
                    {
                      !!this.props.veiculos.length && cooperados.map(cooperado => {
                        const [veiculo] = this.props.veiculos.filter(
                          v => v.cooperado_vr === cooperado.vr
                        );

                        return (
                          <tr key={cooperado.vr}>
                            <td>{cooperado.vr}</td>
                            <td>{cooperado.cpf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g,'\$1.\$2.\$3\-\$4')}</td>
                            <td>{cooperado.nome}</td>
                            <td>
                              {cooperado.telefone_res.replace(/^(\d\d)(\d)/g,'($1) $2').replace(/(\d{4})(\d)/,'$1-$2')}
                            </td>
                            <td>
                              {cooperado.telefone_cel.replace(/^(\d\d)(\d)/g,'($1) $2').replace(/(\d{5})(\d)/,'$1-$2')}
                            </td>
                            <td>{veiculo && veiculo.fabricante} {veiculo && veiculo.modelo}</td>
                            <td>{veiculo && veiculo.placa.replace(/(\w{3})(\d)/, '$1-$2')}</td>
                            <td>{veiculo && veiculo.cor}</td>
                          </tr>
                        );
                      })
                    }
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  cooperados: store.cooperadosReducer.cooperados,
  veiculos: store.veiculosReducer.veiculos,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ setCooperados, setVeiculos }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Cooperados);
