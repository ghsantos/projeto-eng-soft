import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setUser } from '../../../actions';
import {
  Alert,
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row
} from 'reactstrap';
import { withFormik } from 'formik';
import * as Yup from 'yup';

import http from '../../../lib/http';

const UserForm = (props) => {
  const {
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
  } = props;

  return(
    <form onSubmit={handleSubmit}>
      <InputGroup className="mb-3">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="icon-user"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name="email"
          type="text"
          placeholder="Email"
          value={values.email}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.email && touched.email}
        />
        <FormFeedback>Digite um email válido</FormFeedback>
      </InputGroup>
      <InputGroup className="mb-4">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className="icon-lock"></i>
          </InputGroupText>
        </InputGroupAddon>
        <Input
          name='senha'
          type='password'
          placeholder='Senha'
          value={values.senha}
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={errors.senha && touched.senha}
        />
        <FormFeedback>Digite uma senha</FormFeedback>
      </InputGroup>

      <Button type="submit" color="primary" className="px-4" disabled={isSubmitting}>
        {isSubmitting ? 'Aguarde' : 'Login'}
      </Button>
    </form>
  );
}

const InputFormik = withFormik({
  mapPropsToValues: (props) => ({
    email: props.user.email,
    senha: props.user.senha,
  }),

  validationSchema: Yup.object().shape({
    email: Yup.string().email('Invalid email address').required('Email is required!'),
    senha: Yup.string().min(2).required('Pass is required!'),
  }),

  handleSubmit: (values, { setSubmitting, props }) =>
    props.onSubmit(values.email, values.senha, setSubmitting),

})(UserForm);

class Login extends Component {
  state = {
    error: false,
    errorMgs: '',
  }

  submit = (email, senha, setSubmitting) => {
    http.post('/login', {
      email,
      senha,
    }).then((res) => {
      setSubmitting(false);

      this.props.setUser(res.data.funcionario);
    }).catch(err => {
      console.log({err});

      this.setState({ error: true, errorMgs: err.response.data.message });
      setSubmitting(false);
    });
  }

  render() {
    if (this.props.user) {
      return <Redirect to='administracao' />;
    }

    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <h1>Login</h1>
                    <p className="text-muted">Faça login na sua conta</p>
                    <InputFormik user={{ email: '', senha: '' }} onSubmit={this.submit} />

                    { this.state.error &&
                      <Alert className="mb-4" color="danger">
                        {this.state.errorMgs}
                      </Alert>
                    }
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  user: store.userReducer.user,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ setUser }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Login);
