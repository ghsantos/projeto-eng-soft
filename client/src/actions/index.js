export const setUser = user => ({
  type: 'SET_USER',
  user,
});

export const userLogout = () => ({
  type: 'SET_USER',
  user: false,
});

export const setClientes = clientes => ({
  type: 'SET_CLIENTES',
  clientes,
});

export const setEnderecos = enderecos => ({
  type: 'SET_ENDERECOS',
  enderecos,
});

export const setCooperados = cooperados => ({
  type: 'SET_COOPERADOS',
  cooperados,
});

export const setVeiculos = veiculos => ({
  type: 'SET_VEICULOS',
  veiculos,
});

export const setCorridas = corridas => ({
  type: 'SET_CORRIDAS',
  corridas,
});
