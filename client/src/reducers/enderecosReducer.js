const initialState = {
  enderecos: [],
};

export const enderecosReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_ENDERECOS':
      return {
        ...state,
        enderecos: action.enderecos
      };
    default:
      return state;
  }
}
