const initialState = {
  veiculos: [],
};

export const veiculosReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_VEICULOS':
      return {
        ...state,
        veiculos: action.veiculos,
      };
    default:
      return state;
  }
}
