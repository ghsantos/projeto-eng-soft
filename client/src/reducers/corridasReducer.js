const initialState = {
  corridas: [],
};

export const corridasReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_CORRIDAS':
      return {
        ...state,
        corridas: action.corridas
      };
    default:
      return state;
  }
}
