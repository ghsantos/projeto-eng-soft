import { combineReducers } from 'redux';

import { clientesReducer } from './clientesReducer';
import { cooperadosReducer } from './cooperadosReducer';
import { corridasReducer } from './corridasReducer';
import { enderecosReducer } from './enderecosReducer';
import { userReducer } from './userReducer';
import { veiculosReducer } from './veiculosReducer';

export const Reducers = combineReducers({
  clientesReducer,
  cooperadosReducer,
  corridasReducer,
  enderecosReducer,
  userReducer,
  veiculosReducer,
});
