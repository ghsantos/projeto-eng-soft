const initialState = {
  clientes: [],
};

export const clientesReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_CLIENTES':
      return {
        ...state,
        clientes: action.clientes
      };
    default:
      return state;
  }
}
