const initialState = {
  cooperados: [],
};

export const cooperadosReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_COOPERADOS':
      return {
        ...state,
        cooperados: action.cooperados,
      };
    default:
      return state;
  }
}
